- 👋 Hi, I’m @oussama-aouini
- 👀 I’m interested in DataScience & Computer vision
- 🌱 I’m currently learning MERN Stack & ML
- 💞️ I’m looking to collaborate on any cool thing
- 📫 You can reach me on twitter @aouiniou 

<!---
oussama-aouini/oussama-aouini is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
